# OpenML dataset: COMET_MC

https://www.openml.org/d/5889

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: COMET collaboration  
**Acknowledgements**: Chen WU, Ewen Gillies  
**Source**: Unknown - Date unknown
**Please cite**: Monte-Carlo simulation of COMET detector, COMET collaboration, http://comet.kek.jp/

## Guess which points belong to signal track

[COMET](http://comet.kek.jp/Introduction.html) is an experiment being constructed at the J-PARC proton beam laboratory in Japan. It will search for coherent neutrino-less conversion of a muon to an electron, &mu;- + N(A,Z) &rarr; e- + N(A,Z). This process breaks the law of lepton conservation. If detected, it will be a signal of new physics.

The previous upper limit for this decay was set [5] by the SINDRUM II experiment in 2006. COMET is designed to have 10,000 times better sensitivity.

Wires positions are available in a [supplementary file](https://drive.google.com/file/d/0B_gdsqrqzUJyMHcyUVFHa05FLXc/view?usp=sharing)

## Cylindrical Drift Chamber
The COMET experiment is looking for muon to electron conversion, &amp;amp;mu;- + N &amp;amp;rarr; e- + N. COMET Phase-I will the Cylindrical Drift Chamber as the primary detector for physics measurements. Specifically, the momentum of resulting particles will be measured using the CyDet, which is a cylindrical wire array detector.

The particles flying out of muon-stopping target and registered by the CyDet. Among those we are interested in tracks left by electrons with specific energy, which are produced by muon to electron conversion.

The CyDet consists of 4482 sensitive wires organized in 18 layers. Each wire measures the energy deposited by a passing charged particle. Within each of the layers, the wires have same distance to the stopping target and stereometry angle. 

![Scheme of COMET cylindrical detector](https://kaggle2.blob.core.windows.net/competitions/inclass/4520/media/comet_3d.gif)

There is magnetic field in the detector, which causes electron moves in helical path as shown below. This electron deposits energy in the wires close to the flight path. The radius of helix is proportional to transverse momentum of the electron:

R = p_t/(eB)

where p_t is transverse momentum, B is strength of magnetic field, e is charge of electron.

![Trajectory of electron in margetic field](https://kaggle2.blob.core.windows.net/competitions/inclass/4520/media/COMEThelixing.png)

The energy deposited on each wire is measured at the end plate of the cylindrical detector. An example of the resulting signal event can be seen below, where blue dots are background hits and red are hits from signal electrons:

![Energy depositions in COMET](https://kaggle2.blob.core.windows.net/competitions/inclass/4520/media/COMET2dprojection.png)

## Data format
Format of data
Information about position of straw tubes is kept in [supplementary file](https://drive.google.com/file/d/0B_gdsqrqzUJyMHcyUVFHa05FLXc/view?usp=sharing), it has following information:

1. wire_id - zero-based index of straw tubes
2. wire_rho - distance to stopping target
3. wire_phi - phi angle in the plane perpendicular to beam

Each event is a 'snapshot' of detector, it consists of data taken from all 4482 wires: energy and relative time.

Event is written only when hodoscope detects particle. Relative time is difference between time when straw tube detected energy and time of hodoscope.

Data in train.csv is organized as following:

1. event_id - zero-based index of event
2. wire_id - zero-based index of wire
3. global_id - zero-based identifier for each entry in file
4. energy - energy deposited at wire 
5. relative_time = ( time of readout - hodoscope time)
6. label (0 = not activated or background, 1=signal hit)
Wire with zero energy deposition is not activated.

## More details
1. [COMET official site](http://comet.kek.jp/)
2. [COMET conceptual design report](http://comet.kek.jp/Documents_files/comet-cdr-v1.0.pdf)
3. [Раритеты микромира](https://nplus1.ru/news/2015/05/29/reareevents)  - if you aren't deep into HEP, this article in Russian is probably good starting point to understand what is COMET about. 
4. [COMET presentation](http://www-physics.lbl.gov/seminars/old/LBNL2014KUNO.pdf)
5. [A search for &amp;mu;-e conversion in muonic gold](http://www.researchgate.net/publication/226763791_A_search_for_-e_conversion_in_muonic_gold)

## Important note
Datasets available for this challenge are results of preliminary Monte Carlo simulation. They don't completely represent properties of COMET's detector and thus cannot be used to estimate final properties of tracking system, but are appropriate to test different approaches to tracking.

## Acknowledgements
We thank COMET collaboration (and specially Chen WU) for allowing us to use this dataset.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/5889) of an [OpenML dataset](https://www.openml.org/d/5889). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/5889/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/5889/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/5889/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

